package app.repository;

import org.springframework.data.repository.CrudRepository;

import app.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	
	
}
