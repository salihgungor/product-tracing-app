package app.repository;

import org.springframework.data.repository.CrudRepository;

import app.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

	
	
}
