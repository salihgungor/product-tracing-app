package app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Product {

	@Id
	@GeneratedValue
	private int id;

	private String name;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;


}
