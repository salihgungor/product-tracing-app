package app.model;

import java.util.List;

import javax.persistence.*;


import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@Entity
public class Customer {

	@Id
	@GeneratedValue
	private int id;

	private String name;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	private List<Product> products;


	
	

}
