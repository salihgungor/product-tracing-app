package app.controller;

import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import app.model.Customer;
import app.model.Product;
import app.service.CustomerService;
import app.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private CustomerService customerService;

	Logger logger = Logger.getLogger(ProductController.class.getName());

	@GetMapping("/product")
    public Iterable<Product> getProduct() {
    	
    	logger.info("get product.");

    	return productService.getAllProduct();
    }

    // TODO: @RequestBody JSONObject türünde kabul etmiyor.

    @PostMapping("/product/add")
    public Iterable<Product> addProduct(@RequestBody Map<String, Object> payload) {

    	Customer customer = customerService.getCustomerById((Integer) payload.get("customer_id"));

		Map<String, Object> newProduct = (Map<String, Object>) payload.get("product");

    	Product product = new Product();
    	product.setName((String) newProduct.get("name"));
    	product.setCustomer(customer);
    	
    	productService.addProduct(product);

    	logger.info("add product.");
    	
    	return productService.getAllProduct();
    }
    
	@DeleteMapping("/product/{id}")
    public Iterable<Product> deleteProductById(@PathVariable int id) {
    	
    	productService.deleteProductById(id);
    	
    	logger.info("delete product.");
    	
    	return productService.getAllProduct();
    }
	
}
