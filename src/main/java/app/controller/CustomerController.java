package app.controller;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import app.model.Customer;
import app.service.CustomerService;

@RestController
public class CustomerController {

	Logger logger = Logger.getLogger(HomeController.class.getName());

	// TODO: @Autowired 1. kullanım
	@Autowired
	private CustomerService customerService;

/*	// TODO: @Autowired 2. kullanım
	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	// TODO: @Autowired 3. kullanım
	@Autowired
	void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}*/

	// TODO: @GetMapping temelinde @RequestMapping'i kullanıyor.

	@GetMapping("/customer")
    public Iterable<Customer> getCustomer() {
    	
    	logger.info("get customer.");

    	return customerService.getCustomers();
    }
	
	@PostMapping("/customer/add")
    public Iterable<Customer> addCustomer(@RequestBody Customer newCustomer) {
    	
    	customerService.addCustomer(newCustomer);
    	
    	logger.info("add customer.");
    	
    	return customerService.getCustomers();
    }

	@DeleteMapping("/customer/{id}")
    public void deleteCustomerById(@PathVariable int id) {
    	
    	customerService.deleteCustomerById(id);
    	
    	logger.info("delete customer.");
    	
    }
    
	@DeleteMapping("/customer/all")
    public Iterable<Customer> deleteAllCustomer() {
    	
    	customerService.deleteAllCustomer();
    	
    	logger.info("delete all customer.");
    	
    	return customerService.getCustomers();
    }
    
	@PutMapping("/customer/{id}/{name}")
    public Iterable<Customer> updateCustomer(@PathVariable int id, @PathVariable String name) {
    	
    	customerService.updateCustomer(id, name);
    	
    	logger.info("update customer.");
    	
    	return customerService.getCustomers();
    }

}
