package app.controller;

import java.util.logging.Logger;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	Logger logger = Logger.getLogger(HomeController.class.getName());
	
    @RequestMapping("/home")
    public String home() {
    	
    	logger.info("running home controller.");
    	
    	return "This is home controller.";
    }
	
}
