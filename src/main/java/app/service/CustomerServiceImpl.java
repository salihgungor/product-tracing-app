package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.Customer;
import app.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public void addCustomer(Customer customer) {
		
		customerRepository.save(customer);	
	}

	@Override
	public Iterable<Customer> getCustomers() {
		
		return customerRepository.findAll();
	}

	@Override
	public Customer getCustomerById(int id) {
		
		return customerRepository.findById(id).get();
	}

	@Override
	public void deleteCustomerById(int id) {
		
		customerRepository.deleteById(id);
		
	}

	@Override
	public void deleteAllCustomer() {
		
		customerRepository.deleteAll();
	}

	@Override
	public void updateCustomer(int id, String customerName) {
		
		Customer customer = getCustomerById(id);
		customer.setName(customerName);
		customerRepository.save(customer);
		
	}
	
	

}
