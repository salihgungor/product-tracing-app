package app.service;

import java.util.Optional;

import app.model.Product;

public interface ProductService {

	public Iterable<Product> getAllProduct();
	
	public void addProduct(Product product);
	
	public void deleteProductById(int id);
	
}
