package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.Product;
import app.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
    private ProductRepository productRepository;

	@Override
	public Iterable<Product> getAllProduct() {
				
		return productRepository.findAll();
	}

	@Override
	public void addProduct(Product product) {
		
		productRepository.save(product);
		
	}

	@Override
	public void deleteProductById(int id) {
		
		productRepository.deleteById(id);
		
	}
	
	
	
}
