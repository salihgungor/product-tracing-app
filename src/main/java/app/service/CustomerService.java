package app.service;

import app.model.Customer;

public interface CustomerService {

	void addCustomer(Customer customer);

	Iterable<Customer> getCustomers();
	
	Customer getCustomerById(int id);
	
	void deleteCustomerById(int id);
	
	void deleteAllCustomer();
	
	void updateCustomer(int id,  String customerName);
	
}
