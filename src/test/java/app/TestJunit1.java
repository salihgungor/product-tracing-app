package app;

import app.helpers.MessageUtilTest;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import org.junit.Test;

public class TestJunit1 {

    @Test
    public void testSuite() {

        TestSuite suite = new TestSuite(TestJunit1.class, MessageUtilTest.class);
        TestResult result = new TestResult();
        suite.run(result);
        System.out.println("Number of test cases = " + result.runCount());

    }

}
