package app.helpers;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class MessageUtilTest extends TestCase {

    String message = "Hello World";
    MessageUtil messageUtil = new MessageUtil(message);

    int value1;
    int value2;

    @Before
    public void setUp() throws Exception {
        value1 = 3;
        value2 = 3;
    }

    @Test
    public void printMessage() {
        assertEquals(message, messageUtil.printMessage());

        assertTrue((value1 + value2) == 6);
    }


}
