package app;

import junit.framework.TestCase;
import org.junit.Test;

public class TestJunit2 extends TestCase {

    @Test
    public void testAdd() {

        //count the number of test cases
        System.out.println("No of Test Case = "+ this.countTestCases());

        //test getName
        String name = this.getName();
        System.out.println("Test Case Name = "+ name);

        //test setName
        this.setName("testNewAdd");
        String newName = this.getName();
        System.out.println("Updated Test Case Name = "+ newName);

    }

}
