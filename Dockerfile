FROM openjdk:8-jre-alpine

MAINTAINER  Salih Güngör <salih.gungor@pia-team.com>

COPY target/product-tracing-app-0.0.1-SNAPSHOT.jar /

EXPOSE 8080

CMD java -jar ./product-tracing-app-0.0.1-SNAPSHOT.jar
