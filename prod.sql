--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Ubuntu 11.3-0ubuntu0.19.04.1)
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: prodtrace; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE prodtrace WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE prodtrace OWNER TO postgres;

\connect prodtrace

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Categories" (
    category_id integer NOT NULL,
    category_name text NOT NULL,
    category_sub_id integer NOT NULL
);


ALTER TABLE public."Categories" OWNER TO postgres;

--
-- Name: Customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Customers" (
    customer_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."Customers" OWNER TO postgres;

--
-- Name: Departments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Departments" (
    department_id integer NOT NULL,
    department_name text NOT NULL
);


ALTER TABLE public."Departments" OWNER TO postgres;

--
-- Name: Levels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Levels" (
    level_id text NOT NULL,
    level_definition text NOT NULL
);


ALTER TABLE public."Levels" OWNER TO postgres;

--
-- Name: Process; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Process" (
    process_id integer NOT NULL,
    process_num integer NOT NULL,
    process_definition text
);


ALTER TABLE public."Process" OWNER TO postgres;

--
-- Name: Products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Products" (
    product_id integer NOT NULL,
    product_name text NOT NULL,
    process_id integer NOT NULL,
    definition text,
    customer_id integer NOT NULL
);


ALTER TABLE public."Products" OWNER TO postgres;

--
-- Name: Staffs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Staffs" (
    staff_id integer NOT NULL,
    name_surname text NOT NULL,
    deparment_id integer NOT NULL,
    email text NOT NULL,
    manager_id integer,
    level_id integer NOT NULL
);


ALTER TABLE public."Staffs" OWNER TO postgres;

--
-- Name: Tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Tasks" (
    task_id integer NOT NULL,
    task_title text NOT NULL,
    task_definition text NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public."Tasks" OWNER TO postgres;

--
-- Name: Works; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Works" (
    works_id integer NOT NULL,
    product_id integer NOT NULL,
    staff_id integer NOT NULL,
    task_id integer NOT NULL
);


ALTER TABLE public."Works" OWNER TO postgres;

--
-- Name: Categories Categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT "Categories_pkey" PRIMARY KEY (category_id);


--
-- Name: Customers Customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Customers"
    ADD CONSTRAINT "Customers_pkey" PRIMARY KEY (customer_id);


--
-- Name: Departments Departments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Departments"
    ADD CONSTRAINT "Departments_pkey" PRIMARY KEY (department_id);


--
-- Name: Levels Levels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Levels"
    ADD CONSTRAINT "Levels_pkey" PRIMARY KEY (level_id);


--
-- Name: Process Process_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Process"
    ADD CONSTRAINT "Process_pkey" PRIMARY KEY (process_id);


--
-- Name: Products Products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Products"
    ADD CONSTRAINT "Products_pkey" PRIMARY KEY (product_id);


--
-- Name: Staffs Staffs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Staffs"
    ADD CONSTRAINT "Staffs_pkey" PRIMARY KEY (staff_id);


--
-- Name: Tasks Tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tasks"
    ADD CONSTRAINT "Tasks_pkey" PRIMARY KEY (task_id);


--
-- Name: Works Works_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Works"
    ADD CONSTRAINT "Works_pkey" PRIMARY KEY (works_id);


--
-- Name: Categories unique_Category_category_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT "unique_Category_category_id" UNIQUE (category_id);


--
-- Name: Customers unique_Customer_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Customers"
    ADD CONSTRAINT "unique_Customer_id" UNIQUE (customer_id);


--
-- Name: Departments unique_Department_department_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Departments"
    ADD CONSTRAINT "unique_Department_department_id" UNIQUE (department_id);


--
-- Name: Levels unique_Level_level_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Levels"
    ADD CONSTRAINT "unique_Level_level_id" UNIQUE (level_id);


--
-- Name: Process unique_Process_process_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Process"
    ADD CONSTRAINT "unique_Process_process_id" UNIQUE (process_id);


--
-- Name: Products unique_Products_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Products"
    ADD CONSTRAINT "unique_Products_id" UNIQUE (product_id);


--
-- Name: Staffs unique_Staff_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Staffs"
    ADD CONSTRAINT "unique_Staff_id" UNIQUE (staff_id);


--
-- Name: Tasks unique_Tasks_task_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tasks"
    ADD CONSTRAINT "unique_Tasks_task_id" UNIQUE (task_id);


--
-- Name: Works unique_Works_int; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Works"
    ADD CONSTRAINT "unique_Works_int" UNIQUE (works_id);


--
-- PostgreSQL database dump complete
--

